Name:           activemq-cpp
Version:        3.9.5
Release:        7%{?dist}
Summary:        C++ implementation of JMS-like messaging client

Group:          Development/Libraries
License:        ASL 2.0
URL:            https://activemq.apache.org/cms/
Source0:        https://www.apache.org/dist/activemq/activemq-cpp/%{version}/activemq-cpp-library-%{version}-src.tar.gz
Patch0:         activemq-cpp-3.8.2-system-zlib.patch
Patch1:         activemq-TT060952-fix-sigsev-in-transportfilter.patch
Patch2:         activemq-cpp-thread-handle-leak.patch

BuildRequires:  gcc-c++
BuildRequires:  autoconf
BuildRequires:  libtool
BuildRequires:  openssl-devel
BuildRequires:  zlib-devel
BuildRequires:  apr-util-devel >= 1.3
BuildRequires:  cppunit-devel >= 1.10.2
BuildRequires:  libuuid-devel

%description
activemq-cpp is a JMS-like API for C++ for interfacing with Message
Brokers such as Apache ActiveMQ.  C++ messaging service helps to make your
C++ client code much neater and easier to follow. To get a better feel for
CMS try the API Reference.
ActiveMQ-CPP is a client only library, a message broker such as Apache
ActiveMQ is still needed for your clients to communicate.

%package devel
Summary:        C++ implementation header files for JMS-like messaging
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       libuuid-devel

%description devel
activemq-cpp is a JMS-like API for C++ for interfacing with Message
Brokers such as Apache ActiveMQ.  C++ messaging service helps to make
your C++ client code much neater and easier to follow. To get a better
feel for CMS try the API Reference.  ActiveMQ-CPP is a client only
library, a message broker such as Apache ActiveMQ is still needed
for your clients to communicate.

%{name}-devel contains development header files.

%prep
%autosetup -p1 -n activemq-cpp-library-%{version}
rm -r src/main/decaf/internal/util/zip
chmod 644 LICENSE.txt
chmod 644 src/main/activemq/transport/mock/MockTransport.cpp

%build
%configure --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install
rm $RPM_BUILD_ROOT/%{_libdir}/lib%{name}.la
rm $RPM_BUILD_ROOT/%{_bindir}/example

%check
%__make %{?_smp_mflags} check

%files
%{_libdir}/lib%{name}.so.*
%doc LICENSE.txt  NOTICE.txt  README.txt  RELEASE_NOTES.txt

%files devel
%{_libdir}/lib%{name}.so
%{_includedir}/%{name}-%{version}
%{_libdir}/pkgconfig/%{name}.pc
%{_bindir}/activemqcpp-config

%changelog
* Fri Feb 18 2022 Milivoje Legenovic <milivoje.legenovic@profidata.com> - 3.9.5-7
- xdev-6 build

* Sat Jun 26 2021 Levent Demirörs <levent.demiroers@profidata.com> - 3.9.5-6
- Upgrade to GCC 10

* Tue May 04 2021 Levent Demirörs <levent.demiroers@profidata.com> - 3.9.5-5
- Reference specific cppunit version to avoid C++11 issues

* Mon Jan 11 2021 Levent Demirörs <levent.demiroers@profidata.com> - 3.9.5-4
- Add thread handle leak patch

* Wed Dec 16 2020 Levent Demirörs <levent.demiroers@profidata.com> - 3.9.5-3
- Replace patch ACTIVEMQ-TT060952 and ACTIVEMQ-646 w/ unified patch with additional fixes

* Wed Nov 25 2020 Levent Demirörs <levent.demiroers@profidata.com> - 3.9.5-2
- Add patch ACTIVEMQ-TT060952
- Remove activemq-3.8.2 build
- Add chrpath to fix RPATH error in check state

* Tue May 28 2019 Levent Demirörs <levent.demiroers@profidatagroup.com> - 3.9.5-1
- Add patch ACTIVEMQ-646

* Fri Feb 17 2017 Steve Traylen <steve.traylen@cern.ch> - 3.9.3-3
- Correct inter dependecy.

* Wed Feb 15 2017 Steve Traylen <steve.traylen@cern.ch> - 3.9.3-3
- Upstream to 3.9.3
- Add activemqcpp-lib3.8 package to provide old version so.

* Wed Aug 27 2014 Steve Traylen <steve.traylen@cern.ch> - 3.8.4-1
- Upstream to 3.8.3

* Fri Aug 15 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.8.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.8.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu May 29 2014 Ville Skyttä <ville.skytta@iki.fi> - 3.8.2-2
- Patch to use system zlib instead of bundled one

* Thu Jan 23 2014 Steve Traylen <steve.traylen@cern.ch> - 3.8.2-1
- Upstream to 3.8.2

* Wed Sep 4 2013 Steve Traylen <steve.traylen@cern.ch> - 3.7.1-1
- Upstream to 3.7.1

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Sep 16 2012 Steve Traylen <steve.traylen@cern.ch> - 3.4.4-1
- Upstream to 3.4.4

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Feb 28 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.1-2
- Rebuilt for c++ ABI breakage

* Sun Feb 12 2012 Steve Traylen <steve.traylen@cern.ch> - 3.4.1-1
- Upstream to 3.4.1
- Add patch for gcc47, AMQCPP-389

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sun Jul 10 2011 Steve Traylen <steve.traylen@cern.ch> - 3.4.0-1
- Upstream to 3.4.0

* Mon Apr 18 2011 Steve Traylen <steve.traylen@cern.ch> - 3.3.0-1
- Upstream to 3.3.0

* Mon Mar 7 2011 Steve Traylen <steve.traylen@cern.ch> - 3.2.5-1
- autoconf step removed.
- Upstream to 3.2.5

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Feb 6 2011 Steve Traylen <steve.traylen@cern.ch> - 3.2.4-1
- Upstream to 3.2.4

* Wed Nov 10 2010 Steve Traylen <steve.traylen@cern.ch> - 3.2.3-1
- Upstream to 3.2.3

* Thu Jul 22 2010 Steve Traylen <steve.traylen@cern.ch> - 3.2.1-1
- Upstream to 3.2.1
- Add BR of openssl-devel since library now supports ssl
  connections.

* Sat Apr 3 2010 Steve Traylen <steve.traylen@cern.ch> - 3.1.2-1
- Upstream to 3.1.2

* Sat Jan 9 2010 Steve Traylen <steve.traylen@cern.ch> - 3.1.0-1
- Upstream to 3.1.0

* Fri Dec 11 2009 Steve Traylen <steve.traylen@cern.ch> - 3.0.1-1
- Upstream to 3.0.1
- Tar ball name change.

* Fri Dec 11 2009 Steve Traylen <steve.traylen@cern.ch> - 2.2.6-5
- Add libuuid-devel as Requires to -devel package.

* Sat Nov 14 2009 Steve Traylen <steve.traylen@cern.ch> - 2.2.6-4
- Remove patch to relocate headers from versioned directory.
- Add make smp options to make check.

* Fri Nov 6 2009 Steve Traylen <steve.traylen@cern.ch> - 2.2.6-3
- Relocate headers to non versioned directory with patch0

* Fri Nov 6 2009 Steve Traylen <steve.traylen@cern.ch> - 2.2.6-2
- Adapted to Fedora guidelines.

* Thu Feb 26 2009 Ricardo Rocha <ricardo.rocha@cern.ch> - 2.2.6-1
- First version of the spec file


